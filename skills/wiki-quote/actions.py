import wikiquote
import random
import re
from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher


class ActionWikiQuote(Action):
    def __init__(self):
        super(ActionWikiQuote, self).__init__()
        self.exclude_list = ['ISBN', 'Citatum']

    def name(self) -> Text:
        return "action_wiki_quote"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text="Hello World!")
        return []

    def getRandomQuote(self, titles, mostRelevant=False, length=4, filterYear=True):
        quote = None
        quotes = None
        match = None
        counter = 0
        max_sentences = length
        while quote is None or len(quote.split('.')) > max_sentences:
            counter += 1
            if not mostRelevant:
                title = random.choice(titles)
            else:
                title = titles[0]
            try:
                quotes = wikiquote.quotes(title, lang=self.wikilang)
            except wikiquote.utils.DisambiguationPageException:
                quotes = None
            if quotes and quotes != []:
                quote = random.choice(quotes)
                if filterYear:
                    match = re.match('.*([1-3][0-9]{3})', quote)
                if match:
                    quote = None
                for word in self.exclude_list:
                    if quote and word in quote:
                        quote = None
            if counter > 5:
                quote = ''
        return quote, title

    @intent_file_handler('specific.intent')
    def handle_specific_quote_intent(self, message):
        subject = message.data.get('subject')
        results = wikiquote.search(subject, lang=self.wikilang)
        if len(results) == 0:
            self.speak_dialog("notfound", {'subject': subject})
        else:
            quote, title = self.getRandomQuote(
                results, mostRelevant=True, length=10, filterYear=False)
            if quote == '':
                self.speak_dialog("notfound", {'subject': subject})
            else:
                self.speak(quote + ' (' + title + ')')

    @intent_file_handler('random.intent')
    def handle_random_quote_intent(self, message):
        randomtitles = []
        while randomtitles is None or randomtitles == []:
            randomtitles = wikiquote.random_titles(lang=self.wikilang)
        quote, title = self.getRandomQuote(randomtitles)
        self.speak(quote + ' (' + title + ')')
